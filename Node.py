class Node(object):

    def __init__(self, state, parent=None, action=None, cost=0, value=None):
        self.state = state
        self.parent = parent
        self.cost = cost
        self.value = value
        if parent:
            self.action = parent.state + ' -> ' + state

    def __repr__(self):
        
        return self.state + ', ' + self.action