from DFS import DFS
from Problem import Problem
from Worlds import map_romania

INIT_MAP = "Oradea"
GOAL_MAP = "Bucharest"

print(DFS(Problem(INIT_MAP, GOAL_MAP, map_romania)))