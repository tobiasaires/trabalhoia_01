class Problem(object):
    
    def __init__(self, initial, goal=None, action=None):
        self.initial = initial
        self.goal = goal
        self.action = action

    def goal_test(self, nodeState):
        return nodeState==self.goal

    def actions(self, nodeState):
        return list(self.action[nodeState])
